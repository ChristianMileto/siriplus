// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? newJSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

// MARK: - Welcome
class Ingredienti: Codable {
    let q: String?
    let from, to: Int?
    let more: Bool?
    let count: Int?
    let hits: [Hit]?
    
    init(q: String?, from: Int?, to: Int?, more: Bool?, count: Int?, hits: [Hit]?) {
        self.q = q
        self.from = from
        self.to = to
        self.more = more
        self.count = count
        self.hits = hits
    }
}

// MARK: - Hit
class Hit: Codable {
    let recipe: Recipe?
    let bookmarked, bought: Bool?
    
    init(recipe: Recipe?, bookmarked: Bool?, bought: Bool?) {
        self.recipe = recipe
        self.bookmarked = bookmarked
        self.bought = bought
    }
}

// MARK: - Recipe
class Recipe: Codable {
    let uri: String?
    let label: String?
    let image: String?
    let source: String?
    let url: String?
    let shareAs: String?
    let yield: Int?
    let dietLabels: [String]?
    let cautions, ingredientLines: [String]?
    let ingredients: [Ingredient]?
    let calories, totalWeight: Double?
    let totalTime: Int?
    let totalNutrients, totalDaily: [String: Total]?
    let digest: [Digest]?
    
    init(uri: String?, label: String?, image: String?, source: String?, url: String?, shareAs: String?, yield: Int?, dietLabels: [String]?, healthLabels: [HealthLabel]?, cautions: [String]?, ingredientLines: [String]?, ingredients: [Ingredient]?, calories: Double?, totalWeight: Double?, totalTime: Int?, totalNutrients: [String: Total]?, totalDaily: [String: Total]?, digest: [Digest]?) {
        self.uri = uri
        self.label = label
        self.image = image
        self.source = source
        self.url = url
        self.shareAs = shareAs
        self.yield = yield
        self.dietLabels = dietLabels
        self.cautions = cautions
        self.ingredientLines = ingredientLines
        self.ingredients = ingredients
        self.calories = calories
        self.totalWeight = totalWeight
        self.totalTime = totalTime
        self.totalNutrients = totalNutrients
        self.totalDaily = totalDaily
        self.digest = digest
    }
}

// MARK: - Digest
class Digest: Codable {
    let label, tag: String?
    let schemaOrgTag: SchemaOrgTag?
    let total: Double?
    let hasRDI: Bool?
    let daily: Double?
    let unit: Unit?
    let sub: [Digest]?
    
    init(label: String?, tag: String?, schemaOrgTag: SchemaOrgTag?, total: Double?, hasRDI: Bool?, daily: Double?, unit: Unit?, sub: [Digest]?) {
        self.label = label
        self.tag = tag
        self.schemaOrgTag = schemaOrgTag
        self.total = total
        self.hasRDI = hasRDI
        self.daily = daily
        self.unit = unit
        self.sub = sub
    }
}

enum SchemaOrgTag: String, Codable {
    case carbohydrateContent = "carbohydrateContent"
    case cholesterolContent = "cholesterolContent"
    case fatContent = "fatContent"
    case fiberContent = "fiberContent"
    case proteinContent = "proteinContent"
    case saturatedFatContent = "saturatedFatContent"
    case sodiumContent = "sodiumContent"
    case sugarContent = "sugarContent"
    case transFatContent = "transFatContent"
}

enum Unit: String, Codable {
    case empty = "%"
    case g = "g"
    case iu = "IU"
    case kcal = "kcal"
    case mg = "mg"
    case µg = "µg"
}

enum HealthLabel: String, Codable {
    case alcoholFree = "Alcohol-Free"
    case peanutFree = "Peanut-Free"
    case sugarConscious = "Sugar-Conscious"
    case treeNutFree = "Tree-Nut-Free"
}

// MARK: - Ingredient
class Ingredient: Codable {
    let text: String?
    let weight: Double?
    
    init(text: String?, weight: Double?) {
        self.text = text
        self.weight = weight
    }
}

// MARK: - Total
class Total: Codable {
    let label: String?
    let quantity: Double?
    let unit: Unit?
    
    init(label: String?, quantity: Double?, unit: Unit?) {
        self.label = label
        self.quantity = quantity
        self.unit = unit
    }
}

