
import UIKit
import PMAlertController
import IntentsUI
class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var ciao = self
    var nElement = [URL]()
    var Trigger = [INUIAddVoiceShortcutButton]()

    @IBOutlet weak var intentsTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
        nElement = Methods.sharedInstance.numberFolder()!
        intentsTable.delegate = self
        intentsTable.dataSource = self
        registercell()
        self.intentsTable.allowsSelection = true
        
    }
    
    
    
    func registercell(){
        let register = UINib(nibName: "TableViewCell", bundle: nil)
        
        self.intentsTable.register(register, forCellReuseIdentifier: "CustomCell")
        
    }
     
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nElement.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCell") as? TableViewCell
        let nameCategory = nElement[indexPath.row].lastPathComponent.replacingOccurrences(of: ".intentdefinition", with: "")
        let name = nameCategory.split(separator: "_")
        cell!.lb.text = String(name[0])
        self.Trigger.append((cell?.addSiriButton(intent: Methods.sharedInstance.donateInteraction(classe: String(name[0]))))!)
        cell?.imgv.image = Methods.sharedInstance.getCategory(categoria: String(name[1]))
        return cell!
        
        
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        Trigger[indexPath.row].delegate = self
        Trigger[indexPath.row].sendActions(for: .touchUpInside)
        
    }
    func reNew(){
        //reload application data (renew root view )
        UIApplication.shared.keyWindow?.rootViewController = storyboard!.instantiateViewController(withIdentifier: "View")
    }
    
   
  
    
    
    
    
    
    
    
    


}
extension ViewController: INUIAddVoiceShortcutButtonDelegate {
    func present(_ addVoiceShortcutViewController: INUIAddVoiceShortcutViewController, for addVoiceShortcutButton: INUIAddVoiceShortcutButton) {
        addVoiceShortcutViewController.delegate = self
        addVoiceShortcutViewController.modalPresentationStyle = .formSheet
        present(addVoiceShortcutViewController, animated: true, completion: nil)
    }
    
    func present(_ editVoiceShortcutViewController: INUIEditVoiceShortcutViewController, for addVoiceShortcutButton: INUIAddVoiceShortcutButton) {
        editVoiceShortcutViewController.delegate = self
        editVoiceShortcutViewController.modalPresentationStyle = .formSheet
        present(editVoiceShortcutViewController, animated: true, completion: nil)
    }
}

extension ViewController: INUIAddVoiceShortcutViewControllerDelegate {
    func addVoiceShortcutViewController(_ controller: INUIAddVoiceShortcutViewController, didFinishWith voiceShortcut: INVoiceShortcut?, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func addVoiceShortcutViewControllerDidCancel(_ controller: INUIAddVoiceShortcutViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
}

extension ViewController: INUIEditVoiceShortcutViewControllerDelegate {
    func editVoiceShortcutViewController(_ controller: INUIEditVoiceShortcutViewController, didUpdate voiceShortcut: INVoiceShortcut?, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func editVoiceShortcutViewController(_ controller: INUIEditVoiceShortcutViewController, didDeleteVoiceShortcutWithIdentifier deletedVoiceShortcutIdentifier: UUID) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func editVoiceShortcutViewControllerDidCancel(_ controller: INUIEditVoiceShortcutViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
}
