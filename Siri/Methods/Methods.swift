
import Foundation
import Intents
import UIKit


class Methods {
    static let sharedInstance = Methods()
    
    
    
    func getKcl(testo : String,completion : @escaping(String,String)-> Void){
        let baseUrl = "https://api.edamam.com/"
        let path = "search?q=\(testo)&app_id=0a9bc31f&app_key=abaec3cd575be7a4a47b6ce640b8c572"
        
        print(path)
        let endPoint = APIEndpoint(baseUrl: baseUrl, pathString: path, httpMethod: .get)
        _ =  RequestBuilder.execute(endpoint: endPoint, parameters: nil, headers: nil, contentType: .form, acceptEncoding: .form, urlSession: URLSession.shared, queue: DispatchQueue.main) { (data, response, error) -> (Void) in
            guard let datas = data else{
                return
            }
            do{
                var testo1 = ""
                let json = try JSONDecoder().decode(Ingredienti.self, from: datas)
                for a in (json.hits![0].recipe?.ingredientLines)!{
                    testo1+=a+" . \n"
                }
                let stringImage = json.hits![0].recipe?.image
                if testo1 != ""{
                 completion(testo1,stringImage!)
                }else{
                   completion("i'm sorry, i can't find any recipe for you","null")
                }
                
            }
            catch let e {
                print(e)
                completion("null","")
                
            }
        }
        
    }
    
    func getTopNew(completion : @escaping([Article]?)->Void){
        let baseUrl = "https://newsapi.org/"
        let lg = Locale.current.regionCode
        let path = "v2/top-headlines?country=it&apiKey=5c7d5130b5bb4094b3b9a3c2ce49d816"
        print(path)
        
        let endPoint = APIEndpoint(baseUrl: baseUrl, pathString: path, httpMethod: .get)
        
        _ =  RequestBuilder.execute(endpoint: endPoint, parameters: nil, headers: nil, contentType: .form, acceptEncoding: .form, urlSession: URLSession.shared, queue: DispatchQueue.main) { (data, response, error) -> (Void) in
            guard let datas = data else{
                return
            }
            do{
                let json = try JSONDecoder().decode(topnews.self, from: datas)
 
                completion(json.articles)
        
            }
                catch let e {
                    print(e)
                }
                
            
            }
        }
        
        
        
        
        func numberFolder () -> [URL]?{
            let paths = Bundle.main.urls(forResourcesWithExtension: "intentdefinition", subdirectory: nil, localization: nil)
            return paths
        }
        
        func donateInteraction(classe : String) -> INIntent{
            switch classe {
            case "Ingredients":
                let intent = IngredientsIntent()
                
               return intent
            case "TopNews" :
                let intent = TopNewsIntent()
                return intent
                break
            default:
                break
            }
            
            
           return INIntent()
        }
        
        
        
        func getCategory(categoria : String) -> UIImage{
            
            return UIImage(named: categoria)!
        }
        
}
