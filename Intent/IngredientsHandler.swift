//
//  File.swift
//  Siri
//
//  Created by Silvio Fosso on 18/03/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import Foundation
import Intents

class IngredientsHandler : NSObject,IngredientsIntentHandling{
    func resolveRecipe(for intent: IngredientsIntent, with completion: @escaping (INStringResolutionResult) -> Void) {
        if intent.recipe == nil{
            completion(INStringResolutionResult.needsValue())
        }else{
            completion(INStringResolutionResult.success(with: intent.recipe!))
        }
    }
    
    let activity = NSUserActivity(activityType: "silvio.App.Siri")
   
    func confirm(intent: IngredientsIntent, completion: @escaping (IngredientsIntentResponse) -> Void) {
        completion(IngredientsIntentResponse(code: .ready, userActivity: nil))
    }
    
    
    
    
    func handle(intent: IngredientsIntent, completion: @escaping (IngredientsIntentResponse) -> Void) {
        
        activity.userInfo?["testo"] = intent.recipe
        completion(IngredientsIntentResponse.init(code: .success, userActivity: activity))
    }
   
    
    
}
