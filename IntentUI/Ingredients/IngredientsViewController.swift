//
//  IngredientsViewController.swift
//  IntentUI
//
//  Created by Silvio Fosso on 21/03/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import UIKit

class IngredientsViewController: UIViewController {
    
    @IBOutlet var invoiceView: InvoiceView!
    
   
    
    private let intent: IngredientsIntent
    var name : String = ""
    init(for soupIntent: IngredientsIntent) {
        intent = soupIntent
      
        super.init(nibName: "InvoiceView", bundle: Bundle(for: IngredientsViewController.self))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        invoiceView.ac.startAnimating()
        invoiceView.lbl.textColor = .white
        invoiceView.lbl.numberOfLines = 100
        invoiceView.imgv.contentMode = .scaleToFill
        // Do any additional setup after loading the view.
        Methods.sharedInstance.getKcl(testo:  name) { (testo, immagine) in
            
            let purple = UIColor.black
            let purpleTrans = UIColor.withAlphaComponent(purple)(0.8)
            self.invoiceView.sottoview.backgroundColor = purpleTrans
            let url = URL(string: immagine)
            self.downloadImage(from: url!, testo: testo)
            
        }
    }

    func downloadImage(from url: URL,testo : String) {
        print("Download Started")
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            DispatchQueue.main.async() {
                self.invoiceView.ac.stopAnimating()
                self.invoiceView.lbl.text = testo
                self.invoiceView.imgv.image = UIImage(data: data)
                self.invoiceView.ac.removeFromSuperview()
            }
        }
    }
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
class InvoiceView: UIView{
    @IBOutlet weak var ac: UIActivityIndicatorView!
    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var sottoview: UIView!
    @IBOutlet weak var imgv: UIImageView!
}
