
//
//  IngredientsViewController.swift
//  IntentUI
//
//  Created by Silvio Fosso on 21/03/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import UIKit

class TopNewsview: UIViewController,UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        /*let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? Top
        cell?.imgv.image = immagini[indexPath.row]
        cell?.titolo.text = array[indexPath.row].title
        cell?.descrizione.text = array[indexPath.row].articleDescription*/
        let cell = UITableViewCell()
        cell.textLabel?.textColor = .white
        cell.textLabel?.numberOfLines = 5
        cell.backgroundColor = .black
        
        if indexPath.row%2==0{
            
            cell.textLabel?.font = .boldSystemFont(ofSize: 10)
            cell.textLabel?.text = array[indexPath.row].title
            cell.separatorInset = .init(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
        }
        else{
            cell.textLabel?.text = array[indexPath.row-1].articleDescription
            cell.textLabel?.numberOfLines = 5
        }
        return cell
    }
    
   
    
    
    
    
   
    
    @IBOutlet var topnews: Hobama!
    
    
    var array : [Article]!
    var immagini = [UIImage]()
    private let intent: TopNewsIntent
    
    init(for soupIntent:  TopNewsIntent) {
        intent = soupIntent
        
        super.init(nibName: "XibNews", bundle: Bundle(for: TopNewsview.self))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.topnews.table.allowsSelection = true
        Methods.sharedInstance.getTopNew { (articoli) in
            self.array = articoli
            self.topnews.table.separatorColor = .white
            self.topnews.table.delegate = self
            self.topnews.table.dataSource = self
            self.topnews.table.reloadData()
        }
        
        
        
        
    }
    
    
    func downloadImage(from url: URL,completion:@escaping(UIImage)->Void)  {
        print("Download Started")
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            completion(UIImage(data: data)!)
        }
    }
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
class Hobama: UIView{
    @IBOutlet weak var table: UITableView!
}
